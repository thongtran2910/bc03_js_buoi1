/**
 * Input:
 * - Số ngày làm: 5
 *
 * Step:
 * - S1: tạo biến lưu input
 * - S2: tạo biến lưu tiền lương 1 ngày là 100000
 * - S3: tạo biến lưu output
 * - S4: áp dụng công thức theo đề bài cho
 *
 * Output: 500000
 */

var soNgayLam = 5;

var luongMotNgay = 100000;

var tienLuong = null;

tienLuong = luongMotNgay * soNgayLam;

console.log(tienLuong);
