/**
 * Input:
 * - chiều dài HCN: 5
 * - chiều rộng HCN: 2
 *
 * Step:
 * - S1: tạo 2 biến lưu giá trị của 2 input
 * - S2: tạo 2 biến lưu giá trị của 2 output
 * - S3: áp dụng công thức tính diện tích và chu vi HCN
 *
 * Output:
 * - Diện tích: 10
 * - Chu vi: 14
 */

var chieuDaiHCN = 5;

var chieuRongHCN = 2;

var dienTichHCN = null;

var chuViHCN = null;

dienTichHCN = chieuDaiHCN * chieuRongHCN;
chuViHCN = (chieuDaiHCN + chieuRongHCN) * 2;

console.log("Diện tích HCN: ", dienTichHCN, "Chu vi HCN: ", chuViHCN);
