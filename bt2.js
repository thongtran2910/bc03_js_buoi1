/**
 *Input: 1, 3, 5, 7, 9
 *
 * Step:
 * - S1: tạo 5 biến lưu giá trị của 5 input
 * - S2: tạo 1 biến lưu giá trị của output
 * - S3: áp dụng cách tính giá trị trung bình
 *
 * Output: 5
 */

var num1 = 1;

var num2 = 3;

var num3 = 5;

var num4 = 7;

var num5 = 9;

var soTB = null;

soTB = (num1 + num2 + num3 + num4 + num5) / 5;

console.log(soTB);
