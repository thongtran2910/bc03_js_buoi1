/**
 * Input: 25
 *
 * Step:
 * - S1: tạo biến lưu giá trị input
 * - S2: tạo biến lưu giá trị output
 * - S3: áp dụng cách lấy số hàng đơn vị và hàng chục để tính tổng cả 2 số
 *
 * Output: 7
 */

var input = 25;

var output = null;

output = Math.floor(input / 10) + (input % 10);

console.log(output);
