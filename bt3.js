/**
 * Input: 5
 *
 * Step:
 * - S1: tạo biến lưu giá trị của 1 input
 * - S2: tạo biến lưu giá trị tỷ giá của 1 USD = 23500 VND
 * - S3: tạo biến lưu giá trị của output
 * - S4: áp dụng cách quy đổi từ USD -> VND
 *
 * Output: 117500
 */

var soTienUSD = 5;

var tyGiaUSD = 23500;

var soTienVND = null;

soTienVND = soTienUSD * tyGiaUSD;

console.log(soTienVND);
